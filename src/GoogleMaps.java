
public interface GoogleMaps {

	void getDirections(String src, String destn);
	void findALocation(String location);
	void avoidTolls();

	default int test() {
		return 56;
	}

	static void display() {
		System.out.println("Hello");
	}
}
