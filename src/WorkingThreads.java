
public class WorkingThreads implements Runnable{
	String working_thread_msg;

	public WorkingThreads(String working_thread_msg) {
		this.working_thread_msg=working_thread_msg;
	}
	@Override
	public void run() {
		System.out.println("Curren Thread Name: " + Thread.currentThread().getName() + " Started");
		System.out.println("Message: " + this.working_thread_msg);
		try {
			Thread.sleep(1500);
		}
		catch(InterruptedException e) {
			System.out.println(e);
		}
		System.out.println("Curren Thread Name: " + Thread.currentThread().getName() + " Stopped");
	}
}
