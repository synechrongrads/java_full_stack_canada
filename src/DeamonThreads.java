
public class DeamonThreads extends Thread{
	//These are threads of low priority 
	//These are used to provide services to the user defined threads
	//Its life cycle depends on the user threads.
	// to create it use the setDeamonThreads()

	@Override
	public void run() {
		if(Thread.currentThread().isDaemon()) {
			System.out.println(Thread.currentThread().getName());
			System.out.println("It is a deamon thread");
		}
		else {
			System.out.println(Thread.currentThread().getName());
			System.out.println("It is a userthread");
		}
	}

	public static void main(String[] args) {
		DeamonThreads d1 = new DeamonThreads();
		DeamonThreads d2 = new DeamonThreads();
		d1.setName("Deamon");
		d2.setName("Normal");

		d1.start();
		d1.setDaemon(true);
		d2.start();

	}

}
