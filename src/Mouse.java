
public class Mouse {

	int noofbuttons;
	String mousetype;

	Mouse(int noofbuttons,String mousetype) {
		this.noofbuttons = noofbuttons;
		this.mousetype = mousetype;
	}

	Mouse getIstance() {
		return this;
	}

	Mouse(){
		noofbuttons = 100;
		mousetype = "Special Gaming Mouse";
	}

	Mouse returnMouseObject(Mouse m) {
		return this;
	}
	
	void testObject() {
		Mouse y = returnMouseObject(this);
		System.out.println(y.mousetype);
		System.out.println(y.noofbuttons);
	}

	/*
	 * 		def __init__(self,String[] tst):
	 * 
	 * 
	 * 		c = Car({"10","20"})
	 */


	public static void main(String[] args) {
		Mouse m = new Mouse(3,"wireless");
		Mouse k = m.getIstance();
		System.out.println(m);
		System.out.println(k);
		Mouse l = new Mouse();
		Mouse j = l.getIstance();
		System.out.println(m.noofbuttons);
		System.out.println(m.mousetype);
		System.out.println(l);
		System.out.println(j);
		
		Mouse marvel = new Mouse();
		marvel.testObject();
		
	}

}
