
public class ThreadBeta implements Runnable{

	@Override
	public void run() {
		System.out.println("Your thread is in execution");		
	}
	
	public static void main(String[] args) {
		ThreadBeta tb = new ThreadBeta();
		Thread t1 = new Thread(tb);
		t1.start();
	}
}
