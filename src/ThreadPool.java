import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPool {
	public static void main(String[] args) {
		ExecutorService pool_service = Executors.newFixedThreadPool(4);
		int n = 10;

		for(int i=1;i<=n;i++)
		{
				Runnable r = new WorkingThreads("Executing Task " + i + " of " + n);
				pool_service.execute(r);
			
		}
		pool_service.shutdown();
		
		while(!pool_service.isTerminated()) {}
			System.out.println("Completed execution of all the threads n the pool");
		
	}
}
