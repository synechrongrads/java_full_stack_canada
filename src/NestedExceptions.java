import java.util.Scanner;

public class NestedExceptions {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in)			;
		int a= scan.nextInt();
		int b= scan.nextInt();
		try {
			System.out.println(a/b);
			try {
				String t = "Welcome";
				System.out.println(t.charAt(100));

			}
			catch(StringIndexOutOfBoundsException s) {
				System.out.println(s.getMessage());
			}
		}
		catch(ArithmeticException e) {
			System.out.println(e.getMessage());
		}
	}
}
