
public class FreeDeadLock {
	public static void main(String[] args) {
		FreeDeadLock fd = new FreeDeadLock();
		FreeDeadLock.Conor c = fd.new Conor();
		FreeDeadLock.Muthu m = fd.new Muthu();

		Runnable r1 = new Runnable() {

			@Override
			public void run() {
				synchronized (m) {
					System.out.println(Thread.currentThread().getName() + " locks " + m.getClass().getName());
					m.setAge(20);
					System.out.println("Muthu Age inside the Block 2 :"+m.getAge());
					try {
						Thread.sleep(1200);
					}
					catch(InterruptedException e) {
						System.out.println(e);
					}
					
					synchronized (c) {
						System.out.println(Thread.currentThread().getName() + " locks " + c.getClass().getName());
						c.setAge(20);
					System.out.println("Conors Age inside the Block 1 :"+c.getAge());	
					}
				}				
			}
		};
		
		Runnable r2 = new Runnable() {

			@Override
			public void run() {
				synchronized (m) {
					System.out.println(Thread.currentThread().getName() + " locks " + m.getClass().getName());
					m.setAge(20);
					System.out.println("Muthu Age inside the Block 2 :"+m.getAge());
					try {
						Thread.sleep(1200);
					}
					catch(InterruptedException e) {
						System.out.println(e);
					}
					
					synchronized (c) {
						System.out.println(Thread.currentThread().getName() + " locks "+c.getClass().getName());
						c.setAge(20);
					System.out.println("Conors Age inside the Block 2 :"+c.getAge());	
					}
				}				
			}
		};
		
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		t1.start();
		t2.start();

	}

	private class Conor{
		private int age;

		public int getAge() {
			return this.age;
		}

		public void setAge(int age) {
			this.age = age;
		}

	}
	private class Muthu{
		private int age;

		public int getAge() {
			return this.age;
		}

		public void setAge(int age) {
			this.age = age;
		}
	}
}


