
public class Moverload {


	static void show(float a) {
		System.out.println("Float");
	}
	static void show(double a) {
		System.out.println("Double");
	}
	public static void main(String[] args) {
		show(10.45);
	}
}

// method binding:
// The process of attaching the method implementation to its method call


//Compile Time polymorphism

// Method binding decision is gonna be taken by the compiler based on the
// the arguments passed

// Static binding / Static method dispatch / Early Binding