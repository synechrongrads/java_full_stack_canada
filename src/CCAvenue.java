
public interface CCAvenue {

	void netbanking(String username,String password);
	void creditcardtransaction(long cardno, int cardpin);
	void debitcardtransaction(long cardno, int cardpin);
}
