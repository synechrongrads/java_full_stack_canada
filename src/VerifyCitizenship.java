
public class VerifyCitizenship {

	void verifyCitizenship(String citizenship) throws InvalidCitizenshipException{
		if(citizenship.equals("Canadian")) {
			System.out.println("Citizenship Apprved - Ok to provide Passport");
		}
		else {
			throw new InvalidCitizenshipException();
		}
	}
}
