import java.util.Scanner;

public class PassportAuthority {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your name");
		String name = sc.next();
		System.out.println("Enter your Citizenship");
		String citizen = sc.next();
		VerifyCitizenship verify = new VerifyCitizenship();
		try {
			verify.verifyCitizenship(citizen);
		}
		catch(InvalidCitizenshipException e) {
			System.out.println(e.getMessage());		}
		
	}
}


//Step 1: Create a class that extends RuntimeException/Exception
//Step 2: Override getMessage
//Step 3: In the functionality of the application ,the custom exception object has to be thrown
//Step 4: While implementing the functionality, handle it using try and catch or throws