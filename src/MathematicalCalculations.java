import java.util.Scanner;

public class MathematicalCalculations{
	static int power(int base, int exponent){
		int temp = 1;
		for(int i=exponent;i>=1;i--){
			temp = temp * base;
		}
		return temp;
	}

	static int countdigits(int num){
		int count = 0;
		while(num !=0){
			num /= 10;
			count++;
		}
		return count;
	}

	static int reversethenumber(int number)
	{
		int rev = 0;
		while(number != 0){
			int digit = number % 10;
			rev = rev * 10 + digit;
			number /= 10;
		}
		return rev;
	}

	static double si(int Principle, int duration, float rateofinterest ){
		double interest = (Principle * duration *rateofinterest)/100;
		return interest;
	}
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		do{
			System.out.println("1. Calculate Power");
			System.out.println("2. Print the number of digits");
			System.out.println("3. Reverse the number");
			System.out.println("4. Calculate Simple Interest");
			System.out.println("5. Exit");
			System.out.println("Enter your choice [1-5]");
			int choice = scan.nextInt();
			switch(choice){
			case 1:
				System.out.println("Enter the value for base");
				int base = scan.nextInt();
				System.out.println("Enter the value for exponent");
				int exponent = scan.nextInt();
				int result = power(base, exponent);
				System.out.println(base + " raised to the power of "+ exponent + " is: "+ result);
				break;
			case 2:
				System.out.println("Enter a number");
				int num = scan.nextInt();
				int count = countdigits(num);
				System.out.println("The number " + num + " has " + count + " digits.");
				break;
			case 3:
				System.out.println("Enter a number");
				int n = scan.nextInt();
				int rev = reversethenumber(n);
				System.out.println(" Reverse of "+ n + " is: "+ rev);
				break;
			case 4:
				System.out.println("Enter the value for Principle Amount");
				int principle_amount = scan.nextInt();
				System.out.println("Enter the value for duration (no.of.months)");
				int duration = scan.nextInt();
				System.out.println("Enter the value for rate of interest");
				float interest_rate = scan.nextFloat();
				double roi = si(principle_amount,duration,interest_rate);
				System.out.println(" The Simple Interest for the principle amount "+ principle_amount + " for a period of "+ duration 
						+ " month(s) for the interest rate " + interest_rate + " % is: " + roi);
				break;
			case 5:
				System.out.println("Thank you have a great day");
				return;
			default:
				System.out.println("Incalid choice");
			}
		}while(true);
	}
}