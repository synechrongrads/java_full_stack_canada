
public class InvalidCitizenshipException extends Exception{

	String message = "You are not citizen of Canada and hence cannot provide Canadian Passport";

	@Override
	public String getMessage() {
		return this.message;	}

}
