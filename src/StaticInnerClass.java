
public class StaticInnerClass {

	static class StInner{
		static String name = "CrazyClass";
		int height = 23523;
		
		static void show() {
			System.out.println(name);
		}
		static void check() {
			StInner sti = new StInner();
			System.out.println(sti.height);
		} 
	}
}
