
public class AgeCalculator <C,I>{
	C age;
	I empid;
	
	public AgeCalculator(C age, I empid) {
		this.age = age;
		this.empid = empid;
	}
	
	void calculate() {
		System.out.println("You are "+ this.age + " years(s) old");
		System.out.println("Your empid is: "+ this.empid);
	}
}
