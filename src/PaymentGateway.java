
public class PaymentGateway implements CCAvenue, Razorpay {

	Bank b;

	int amount;
	public PaymentGateway(int amount, Bank b) {
		this.amount = amount;
		this.b=b;
	}
	@Override
	public void netbanking(String username, String password) {
		System.out.println("Welcome " + username);
		b.accountbalance -= this.amount;
		System.out.println("Balance after payment: " + b.accountbalance);

	}

	@Override
	public void creditcardtransaction(long cardno, int cardpin) {
		b.accountbalance -= this.amount;
		System.out.println("Balance after payment: " + b.accountbalance);
	}

	@Override
	public void debitcardtransaction(long cardno, int cardpin) {
		b.accountbalance -= this.amount;
		System.out.println("Balance after payment: " + b.accountbalance);
	}

}
