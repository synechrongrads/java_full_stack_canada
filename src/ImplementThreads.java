
public class ImplementThreads {
public static void main(String[] args) {
	DataTable dt = new DataTable();
	MyThread1 mt1 = new MyThread1(dt);
	MyThread2 mt2 = new MyThread2(dt);
	
	mt1.start();
	mt2.start();
}
}
