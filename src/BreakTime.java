
public class BreakTime {
	void giveBreak(String breaktype) {
		System.out.println(breaktype);
		giveBreak("Lunch", 1);
	}

	void giveBreak(String breaktype, int duration) {
		giveBreak("Dinner","Sherton",3);
		System.out.println(breaktype);
		System.out.println(duration);
	}

	void giveBreak(String breaktype, String location, int duration) {
		giveBreak("Tea Break");
		System.out.println(breaktype);
		System.out.println(duration);
		System.out.println(location);
	}

	public static void main(String[] args) {
		
		//Create object of BreakTime
		BreakTime bt = new BreakTime();
		//call the giveBreak method using object ref
		bt.giveBreak("Tea Break");
	}
}
