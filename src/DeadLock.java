
public class DeadLock {
	public static void main(String[] args) {
		String Conor_fav = "Pizza";
		String Shiraz_fav = "Pan Cakes";

		Thread t1 = new Thread() {
			@Override
			public void run() {


				synchronized (Shiraz_fav) {
					System.out.println(Thread.currentThread().getName() + "Locking " + Shiraz_fav);
					try {
						Thread.sleep(1200);
					}
					catch(InterruptedException e) {
						System.out.println(e);
					}


					synchronized (Conor_fav) {
						System.out.println(Thread.currentThread().getName() +  "Locking " + Conor_fav);
						try {
							Thread.sleep(1200);
						}
						catch(InterruptedException e) {
							System.out.println(e);
						}
					}
				}
			}
		};

		Thread t2 = new Thread() {
			@Override
			public void run() {

				synchronized (Conor_fav) {
					System.out.println(Thread.currentThread().getName() +  "Locking " + Conor_fav);
					try {
						Thread.sleep(1200);
					}
					catch(InterruptedException e) {
						System.out.println(e);
					}


					synchronized (Shiraz_fav) {
						System.out.println(Thread.currentThread().getName() + "Locking " + Shiraz_fav);
						try {
							Thread.sleep(1200);
						}
						catch(InterruptedException e) {
							System.out.println(e);
						}
					}
				}
			}
		};

		t1.start();
		t2.start();

	}
}
