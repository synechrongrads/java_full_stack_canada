
public class DoWork {
	
	static DoWork dd = new DoWork();
	public DoWork() {
		this("Diana");
		dd = new DoWork();
	}
	public DoWork(String name){
		this(name,6734);
		dd = new DoWork();

	}
	public DoWork(String name, int id){
		this(dd);
	}

	public DoWork(DoWork dw){

	}

	// Cyclic chaining is not allowed in Java
	public static void main(String[] args) {
		DoWork d = new DoWork("Welcome");
		System.out.println(d.toString());
		DoWork r = d;
		d = new DoWork();
		System.gc();
	}
}
