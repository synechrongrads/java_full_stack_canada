
public class MultipleExceptions {
	public static void main(String[] args) {
		int a=10;
		int b = 0;
		try {
			System.out.println(a/b);
			String h = null;
			System.out.println(h.charAt(2));
		}
		catch(ArithmeticException e) {
			System.out.println(e.getMessage());
			System.out.println("Entering the second value again");
			b = 2;
			System.out.println(a/b);
		}
		catch(NullPointerException ne) {
			System.out.println(ne.getMessage());
		}
		System.out.println("Bye");
	}
}
