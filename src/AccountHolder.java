
public class AccountHolder implements Runnable {
private Account account;

public AccountHolder(Account account) {
	this.account = account;
}

@Override
public void run() {
	for(int i=1;i<=4;i++) {
		makeWithdrawal(2000);
		if(account.getBalance() < 0) {
			System.out.println("Account Overdrawn");
		}
	}
}

private synchronized void makeWithdrawal(int withdrawalamt){
	if(account.getBalance()>=withdrawalamt) {
		System.out.println(Thread.currentThread().getName() + " initiated the withdrawal process of"
				+"$ " +withdrawalamt);
		try {
			Thread.sleep(2500);
		}
		catch(InterruptedException e) {
			System.out.println(e);
		}
		account.withdrawal(withdrawalamt);
		System.out.println(Thread.currentThread().getName() + " completed the withdrawal process of"
				+"$ " +withdrawalamt);
	}
	else {
		System.out.println("Not enough money present in theaccount to withdraw");
		System.out.println("Available balance: " + account.getBalance());
	}
}

}
