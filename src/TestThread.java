
public class TestThread{

	public static void main(String[] args) {
		try {
			MyThread t1 = new MyThread();
			t1.start();
			t1.interruptedThread = Thread.currentThread();
			
			t1.join();
		}
		catch (InterruptedException e) {
			System.out.println(e);
			
		}


	}
}


class MyThread extends Thread{
	Thread interruptedThread;
	@Override
	public void run() {
		interruptedThread.interrupt();
	}
}